module.exports = {
  purge: [
    "./index.html"
  ],
  theme: {
    extend: {
      colors: {
        brand: {
          100: '#E7F0F0',
          200: '#C4D8D9',
          300: '#A0C1C2',
          400: '#589394',
          450: '#337970',
          500: '#116466',
          600: '#0F5A5C',
          700: '#0A3C3D',
          800: '#082D2E',
          900: '#051E1F',
        },
        grey: '#2c3531'
      }
    },
  },
  variants: {},
  plugins: [],
}
