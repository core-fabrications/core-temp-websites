const { resolve } = require('path');

const HtmlPlugin = require('html-webpack-plugin');
const CssPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const isDev = process.env.NODE_ENV !== 'production';

module.exports = {
  target: 'web',
  entry: {
    main: resolve(process.cwd(), 'src', 'main.js')
  },
  output: {
    filename: '[name].[hash].js',
    chunkFilename: '[name].[id].js',
    path: resolve('dist')
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [{
          loader: CssPlugin.loader,
          options: {
            hmr: isDev
          }
        }, 'css-loader', 'postcss-loader']
      },
      {
        test: /\.(woff2?|svg$)/,
        use: ['file-loader']
      }
    ]
  },
  plugins: [
    new CssPlugin({
      filename: isDev ? '[name].css' : '[name].[hash].css'
    }),
    new HtmlPlugin({
      template: resolve(process.cwd(), 'index.html')
    }),
    new CopyPlugin({
      patterns: [
        {
          from: 'static',
          to: '.'
        }
      ]
    })
  ]
}